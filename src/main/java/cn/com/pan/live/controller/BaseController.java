package cn.com.pan.live.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class BaseController {

	protected final Logger log = LogManager.getLogger(getClass());
}
